defmodule Aoc.Year2020.Day06.CustomCustomsTest do
  use Aoc.DayCase
  doctest Aoc.Year2020.Day06.CustomCustoms, import: true

  alias Aoc.Year2020.Day06.CustomCustoms

  describe "part_1/1" do
    test "examples" do
    end

    @tag day: 06, year: 2020
    test "input", %{input: input} do
      assert input |> CustomCustoms.part_1() == 6249
    end
  end

  describe "part_2/1" do
    test "examples" do
    end

    @tag day: 06, year: 2020
    test "input", %{input: input} do
      assert input |> CustomCustoms.part_2() == 3103
    end
  end
end
