defmodule Aoc.Year2020.Day03.TobogganTrajectoryTest do
  use Aoc.DayCase
  doctest Aoc.Year2020.Day03.TobogganTrajectory, import: true

  alias Aoc.Year2020.Day03.TobogganTrajectory

  describe "part_1/1" do
    @example """
    ..##.......
    #...#...#..
    .#....#..#.
    ..#.#...#.#
    .#...##..#.
    ..#.##.....
    .#.#.#....#
    .#........#
    #.##...#...
    #...##....#
    .#..#...#.#
    """

    test "collision" do
      assert TobogganTrajectory.collision(@example, 1, 3, 1) == 0
      assert TobogganTrajectory.collision(@example, 2, 3, 1) == 1
      assert TobogganTrajectory.collision(@example, 3, 3, 1) == 0
      assert TobogganTrajectory.collision(@example, 4, 3, 1) == 1
      assert TobogganTrajectory.collision(@example, 5, 3, 1) == 1
      assert TobogganTrajectory.collision(@example, 6, 3, 1) == 0
      assert TobogganTrajectory.collision(@example, 7, 3, 1) == 1
      assert TobogganTrajectory.collision(@example, 8, 3, 1) == 1
      assert TobogganTrajectory.collision(@example, 9, 3, 1) == 1
      assert TobogganTrajectory.collision(@example, 10, 3, 1) == 1
    end

    test "part_1 example" do
      assert TobogganTrajectory.part_1(@example) == 7
    end

    test "part_2 example" do
      assert TobogganTrajectory.part_2(@example) == 336
    end

    @tag day: 03, year: 2020
    test "input", %{input: input} do
      assert input |> TobogganTrajectory.part_1() == 230
    end
  end

  describe "part_2/1" do
    test "examples" do
    end

    @tag day: 03, year: 2020
    test "input", %{input: input} do
      assert input |> TobogganTrajectory.part_2() == 9_533_698_720
    end
  end
end
