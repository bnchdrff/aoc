defmodule Aoc.Year2020.Day05.BinaryBoardingTest do
  use Aoc.DayCase
  doctest Aoc.Year2020.Day05.BinaryBoarding, import: true

  alias Aoc.Year2020.Day05.BinaryBoarding

  describe "part_1/1" do
    test "examples" do
    end

    @tag day: 05, year: 2020
    test "input", %{input: input} do
      assert input |> BinaryBoarding.part_1() == 928
    end
  end

  describe "part_2/1" do
    test "examples" do
    end

    @tag day: 05, year: 2020
    test "input", %{input: input} do
      assert input |> BinaryBoarding.part_2() == 610
    end
  end
end
