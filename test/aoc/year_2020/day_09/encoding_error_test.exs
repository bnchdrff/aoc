defmodule Aoc.Year2020.Day09.EncodingErrorTest do
  use Aoc.DayCase
  doctest Aoc.Year2020.Day09.EncodingError, import: true

  alias Aoc.Year2020.Day09.EncodingError

  describe "part_1/1" do
    test "examples" do
    end

    @tag day: 09, year: 2020
    test "input", %{input: input} do
      assert input |> EncodingError.part_1() == 731_031_916
    end
  end

  describe "part_2/1" do
    test "examples" do
    end

    @tag day: 09, year: 2020
    test "input", %{input: input} do
      assert input |> EncodingError.part_2() == 93_396_727
    end
  end
end
