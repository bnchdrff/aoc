defmodule Aoc.Year2020.Day10.AdapterArrayTest do
  use Aoc.DayCase
  doctest Aoc.Year2020.Day10.AdapterArray, import: true

  alias Aoc.Year2020.Day10.AdapterArray

  describe "part_1/1" do
    test "examples" do
    end

    @tag day: 10, year: 2020
    test "input", %{input: input} do
      assert input |> AdapterArray.part_1() == 1848
    end
  end

  describe "part_2/1" do
    test "examples" do
    end

    @tag day: 10, year: 2020
    test "input", %{input: input} do
      assert input |> AdapterArray.part_2() == 8_099_130_339_328
    end
  end
end
