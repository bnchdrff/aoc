defmodule Aoc.Year2020.Day04.PassportProcessingTest do
  use Aoc.DayCase
  doctest Aoc.Year2020.Day04.PassportProcessing, import: true

  alias Aoc.Year2020.Day04.PassportProcessing

  describe "part_1/1" do
    test "examples" do
    end

    @tag day: 04, year: 2020
    test "input", %{input: input} do
      assert input |> PassportProcessing.part_1() == 247
    end
  end

  describe "part_2/1" do
    test "examples" do
    end

    @tag day: 04, year: 2020
    test "input", %{input: input} do
      assert input |> PassportProcessing.part_2() == 145
    end
  end
end
