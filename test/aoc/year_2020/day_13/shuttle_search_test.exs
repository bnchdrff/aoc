defmodule Aoc.Year2020.Day13.ShuttleSearchTest do
  use Aoc.DayCase
  doctest Aoc.Year2020.Day13.ShuttleSearch, import: true

  alias Aoc.Year2020.Day13.ShuttleSearch

  describe "part_1/1" do
    test "examples" do
    end

    @tag day: 13, year: 2020
    test "input", %{input: input} do
      assert input |> ShuttleSearch.part_1() == 333
    end
  end

  describe "part_2/1" do
    test "examples" do
    end

    #  @tag day: 13, year: 2020
    #  test "input", %{input: input} do
    #    assert input |> ShuttleSearch.part_2() == 0
    #  end
  end
end
