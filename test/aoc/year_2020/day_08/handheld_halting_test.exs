defmodule Aoc.Year2020.Day08.HandheldHaltingTest do
  use Aoc.DayCase
  doctest Aoc.Year2020.Day08.HandheldHalting, import: true

  alias Aoc.Year2020.Day08.HandheldHalting

  describe "part_1/1" do
    test "examples" do
    end

    @tag day: 08, year: 2020
    test "input", %{input: input} do
      assert input |> HandheldHalting.part_1() == 2003
    end
  end

  describe "part_2/1" do
    test "examples" do
    end

    @tag day: 08, year: 2020
    test "input", %{input: input} do
      assert input |> HandheldHalting.part_2() == {:done, 1984, 637}
    end
  end
end
