defmodule Aoc.Year2020.Day11.SeatingSystemTest do
  use Aoc.DayCase
  doctest Aoc.Year2020.Day11.SeatingSystem, import: true

  alias Aoc.Year2020.Day11.SeatingSystem

  describe "part_1/1" do
    test "examples" do
    end

    @tag day: 11, year: 2020
    test "input", %{input: input} do
      assert input |> SeatingSystem.part_1() == 2368
    end
  end

  describe "part_2/1" do
    test "examples" do
    end

    @tag day: 11, year: 2020
    test "input", %{input: input} do
      assert input |> SeatingSystem.part_2() == 2124
    end
  end
end
