defmodule Aoc.Year2020.Day12.RainRiskTest do
  use Aoc.DayCase
  doctest Aoc.Year2020.Day12.RainRisk, import: true

  alias Aoc.Year2020.Day12.RainRisk

  describe "part_1/1" do
    test "examples" do
    end

    @tag day: 12, year: 2020
    test "input", %{input: input} do
      assert input |> RainRisk.part_1() == 590
    end
  end

  describe "part_2/1" do
    test "examples" do
    end

    @tag day: 12, year: 2020
    test "input", %{input: input} do
      assert input |> RainRisk.part_2() == 42013
    end
  end
end
