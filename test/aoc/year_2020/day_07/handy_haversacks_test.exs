defmodule Aoc.Year2020.Day07.HandyHaversacksTest do
  use Aoc.DayCase
  doctest Aoc.Year2020.Day07.HandyHaversacks, import: true

  alias Aoc.Year2020.Day07.HandyHaversacks

  describe "part_1/1" do
    test "examples" do
    end

    @tag day: 07, year: 2020
    test "input", %{input: input} do
      assert input |> HandyHaversacks.part_1() == 224
    end
  end

  describe "part_2/1" do
    test "examples" do
    end

    @tag day: 07, year: 2020
    test "input", %{input: input} do
      assert input |> HandyHaversacks.part_2() == 1488
    end
  end
end
