defmodule Aoc.Year2021.Day03.BinaryDiagnosticTest do
  use Aoc.DayCase
  doctest Aoc.Year2021.Day03.BinaryDiagnostic, import: true, tags: :doctest
  doctest LeastCommonIn, import: true, tags: :doctest
  doctest MostCommonIn, import: true, tags: :doctest

  alias Aoc.Year2021.Day03.BinaryDiagnostic

  describe "part_1/1" do
    test "examples" do
      assert 198 =
               """
               00100
               11110
               10110
               10111
               10101
               01111
               00111
               11100
               10000
               11001
               00010
               01010
               """
               |> BinaryDiagnostic.part_1()
    end

    @tag day: 03, year: 2021
    test "input", %{input: input} do
      assert input |> BinaryDiagnostic.part_1() == 2_972_336
    end
  end

  describe "part_2/1" do
    @tag day: 03, year: 2021, example: true
    test "examples" do
      assert 230 =
               """
               00100
               11110
               10110
               10111
               10101
               01111
               00111
               11100
               10000
               11001
               00010
               01010
               """
               |> BinaryDiagnostic.part_2()
    end

    @tag day: 03, year: 2021
    test "input", %{input: input} do
      assert input |> BinaryDiagnostic.part_2() == 3_368_358
    end
  end
end
