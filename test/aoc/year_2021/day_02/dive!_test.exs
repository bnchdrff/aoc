defmodule Aoc.Year2021.Day02.DiveTest do
  use Aoc.DayCase
  doctest Aoc.Year2021.Day02.Dive, import: true

  alias Aoc.Year2021.Day02.Dive

  describe "part_1/1" do
    test "examples" do
      assert """
             forward 5
             down 5
             forward 8
             up 3
             down 8
             forward 2
             """
             |> Dive.part_1() == 150
    end

    @tag day: 02, year: 2021
    test "input", %{input: input} do
      assert input |> Dive.part_1() == 2_036_120
    end
  end

  describe "part_2/1" do
    @tag day: 02, year: 2021, example: true
    test "examples" do
      assert """
             forward 5
             down 5
             forward 8
             up 3
             down 8
             forward 2
             """
             |> Dive.part_2() == 900
    end

    @tag day: 02, year: 2021
    test "input", %{input: input} do
      assert input |> Dive.part_2() == 2015547716
    end
  end
end
