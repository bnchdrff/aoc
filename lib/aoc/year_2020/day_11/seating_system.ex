defmodule Aoc.Year2020.Day11.SeatingSystem do
  @moduledoc """
  ## --- Day 11: Seating System --- 

  Your plane lands with plenty of time to spare. The final leg of your journey is
  a ferry that goes directly to the tropical island where you can finally start
  your vacation. As you reach the waiting area to board the ferry, you realize
  you're so early, nobody else has even arrived yet! 

  By modeling the process people use to choose (or abandon) their seat in the
  waiting area, you're pretty sure you can predict the best place to sit. You make
  a quick map of the seat layout (your puzzle input). 

  The seat layout fits neatly on a grid. Each position is either floor (`.`), an
  empty seat (`L`), or an occupied seat (`#`). For example, the initial seat
  layout might look like this: 

  `L.LL.LL.LL
  LLLLLLL.LL
  L.L.L..L..
  LLLL.LL.LL
  L.LL.LL.LL
  L.LLLLL.LL
  ..L.L.....
  LLLLLLLLLL
  L.LLLLLL.L
  L.LLLLL.LL
  `Now, you just need to model the people who will be arriving shortly.
  Fortunately, people are entirely predictable and always follow a simple set of
  rules. All decisions are based on the *number of occupied seats* adjacent to a
  given seat (one of the eight positions immediately up, down, left, right, or
  diagonal from the seat). The following rules are applied to every seat
  simultaneously: 

  - If a seat is *empty* (`L`) and there are *no* occupied seats adjacent to it, the seat becomes *occupied*. 
  - If a seat is *occupied* (`#`) and *four or more* seats adjacent to it are also occupied, the seat becomes *empty*. 
  - Otherwise, the seat's state does not change. 
  Floor (`.`) never changes; seats don't move, and nobody sits on the floor. 

  After one round of these rules, every seat in the example layout becomes
  occupied: 

  `#.##.##.##
  #######.##
  #.#.#..#..
  ####.##.##
  #.##.##.##
  #.#####.##
  ..#.#.....
  ##########
  #.######.#
  #.#####.##
  `After a second round, the seats with four or more occupied adjacent seats become
  empty again: 

  `#.LL.L#.##
  #LLLLLL.L#
  L.L.L..L..
  #LLL.LL.L#
  #.LL.LL.LL
  #.LLLL#.##
  ..L.L.....
  #LLLLLLLL#
  #.LLLLLL.L
  #.#LLLL.##
  `This process continues for three more rounds: 

  `#.##.L#.##
  #L###LL.L#
  L.#.#..#..
  #L##.##.L#
  #.##.LL.LL
  #.###L#.##
  ..#.#.....
  #L######L#
  #.LL###L.L
  #.#L###.##
  ``#.#L.L#.##
  #LLL#LL.L#
  L.L.L..#..
  #LLL.##.L#
  #.LL.LL.LL
  #.LL#L#.##
  ..L.L.....
  #L#LLLL#L#
  #.LLLLLL.L
  #.#L#L#.##
  ``#.#L.L#.##
  #LLL#LL.L#
  L.#.L..#..
  #L##.##.L#
  #.#L.LL.LL
  #.#L#L#.##
  ..L.L.....
  #L#L##L#L#
  #.LLLLLL.L
  #.#L#L#.##
  `At this point, something interesting happens: the chaos stabilizes and further
  applications of these rules cause no seats to change state! Once people stop
  moving around, you count *`37`* occupied seats. 

  Simulate your seating area by applying the seating rules repeatedly until no
  seats change state. *How many seats end up occupied?* 


  ## --- Part Two --- 

  As soon as people start to arrive, you realize your mistake. People don't just
  care about adjacent seats - they care about *the first seat they can see* in
  each of those eight directions! 

  Now, instead of considering just the eight immediately adjacent seats, consider
  the *first seat* in each of those eight directions. For example, the empty seat
  below would see *eight* occupied seats: 

  `.......#.
  ...#.....
  .#.......
  .........
  ..#L....#
  ....#....
  .........
  #........
  ...#.....
  `The leftmost empty seat below would only see *one* empty seat, but cannot see
  any of the occupied ones: 

  `.............
  .L.L.#.#.#.#.
  .............
  `The empty seat below would see *no* occupied seats: 

  `.##.##.
  #.#.#.#
  ##...##
  ...L...
  ##...##
  #.#.#.#
  .##.##.
  `Also, people seem to be more tolerant than you expected: it now takes *five or
  more* visible occupied seats for an occupied seat to become empty (rather than
  *four or more* from the previous rules). The other rules still apply: empty
  seats that see no occupied seats become occupied, seats matching no rule don't
  change, and floor never changes. 

  Given the same starting layout as above, these new rules cause the seating area
  to shift around as follows: 

  `L.LL.LL.LL
  LLLLLLL.LL
  L.L.L..L..
  LLLL.LL.LL
  L.LL.LL.LL
  L.LLLLL.LL
  ..L.L.....
  LLLLLLLLLL
  L.LLLLLL.L
  L.LLLLL.LL
  ``#.##.##.##
  #######.##
  #.#.#..#..
  ####.##.##
  #.##.##.##
  #.#####.##
  ..#.#.....
  ##########
  #.######.#
  #.#####.##
  ``#.LL.LL.L#
  #LLLLLL.LL
  L.L.L..L..
  LLLL.LL.LL
  L.LL.LL.LL
  L.LLLLL.LL
  ..L.L.....
  LLLLLLLLL#
  #.LLLLLL.L
  #.LLLLL.L#
  ``#.L#.##.L#
  #L#####.LL
  L.#.#..#..
  ##L#.##.##
  #.##.#L.##
  #.#####.#L
  ..#.#.....
  LLL####LL#
  #.L#####.L
  #.L####.L#
  ``#.L#.L#.L#
  #LLLLLL.LL
  L.L.L..#..
  ##LL.LL.L#
  L.LL.LL.L#
  #.LLLLL.LL
  ..L.L.....
  LLLLLLLLL#
  #.LLLLL#.L
  #.L#LL#.L#
  ``#.L#.L#.L#
  #LLLLLL.LL
  L.L.L..#..
  ##L#.#L.L#
  L.L#.#L.L#
  #.L####.LL
  ..#.#.....
  LLL###LLL#
  #.LLLLL#.L
  #.L#LL#.L#
  ``#.L#.L#.L#
  #LLLLLL.LL
  L.L.L..#..
  ##L#.#L.L#
  L.L#.LL.L#
  #.LLLL#.LL
  ..#.L.....
  LLL###LLL#
  #.LLLLL#.L
  #.L#LL#.L#
  `Again, at this point, people stop shifting around and the seating area reaches
  equilibrium. Once this occurs, you count *`26`* occupied seats. 

  Given the new visibility method and the rule change for occupied seats becoming
  empty, once equilibrium is reached, *how many seats end up occupied?* 

  """

  def testdata() do
    """
    L.LL.LL.LL
    LLLLLLL.LL
    L.L.L..L..
    LLLL.LL.LL
    L.LL.LL.LL
    L.LLLLL.LL
    ..L.L.....
    LLLLLLLLLL
    L.LLLLLL.L
    L.LLLLL.LL
    """
  end

  def prepare(input) do
    input
    |> String.split("\n")
    |> Enum.filter(&(&1 != ""))
    |> Enum.map(fn l ->
      l
      |> String.split("")
      |> Enum.filter(&(&1 != ""))
    end)
  end

  def graphit(input) do
    grid =
      input
      |> prepare

    graph = :digraph.new()

    height = length(grid) - 1
    width = length(Enum.at(grid, 0)) - 1

    for row <- 0..height, col <- 0..width do
      case lookup(grid, {row, col}) do
        "L" ->
          add_to_graph(row, col, graph)

          for n <- get_neighbors(row, col, height, width) do
            add_neighbor({row, col}, n, lookup(grid, n), graph)
          end

        _ ->
          nil
      end
    end

    graph
  end

  def lookup(grid, {row, col}), do: grid |> Enum.at(row) |> Enum.at(col)

  def add_to_graph(row, col, g) do
    :digraph.add_vertex(g, {row, col}, occupied: false)
  end

  def get_neighbors(row, col, height, width) do
    [
      {row - 1, col - 1},
      {row - 1, col},
      {row - 1, col + 1},
      {row, col - 1},
      {row, col + 1},
      {row + 1, col - 1},
      {row + 1, col},
      {row + 1, col + 1}
    ]
    |> Enum.filter(fn {row, col} ->
      row >= 0 and col >= 0 and
        row <= height and col <= width
    end)
  end

  def find_in_direction(_, {row, col}, _, max_row, max_col)
      when row > max_row or col > max_col or row < 0 or col < 0,
      do: false

  def find_in_direction(graph, {row, col}, {mod_row, mod_col}, max_row, max_col) do
    case :digraph.vertex(graph, {row, col}) do
      {coords, [occupied: false]} ->
        coords

      _ ->
        next = {row + mod_row, col + mod_col}
        find_in_direction(graph, next, {mod_row, mod_col}, max_row, max_col)
    end
  end

  @doc """

  """
  def get_neighbors_pt2({row, col}, graph, max_row, max_col) do
    for r <- -1..1, c <- -1..1 do
      case find_in_direction(graph, {row + r, col + c}, {r, c}, max_row, max_col) do
        false -> nil
        neighbor -> add_neighbor({row, col}, neighbor, "L", graph)
      end
    end
  end

  def graphit_pt2(input) do
    grid =
      input
      |> prepare

    graph = :digraph.new()

    height = length(grid) - 1
    width = length(Enum.at(grid, 0)) - 1

    for row <- 0..height, col <- 0..width do
      case lookup(grid, {row, col}) do
        "L" ->
          add_to_graph(row, col, graph)

        _ ->
          nil
      end
    end

    for row <- 0..height, col <- 0..width do
      case lookup(grid, {row, col}) do
        "L" ->
          get_neighbors_pt2({row, col}, graph, height, width)

        _ ->
          nil
      end
    end

    graph
  end

  def add_neighbor(_, _, ".", _), do: nil

  def add_neighbor(from, to, _, _) when from == to, do: nil

  def add_neighbor(from, to, _, graph) do
    :digraph.add_edge(graph, from, to)
    :digraph.add_edge(graph, to, from)
  end

  def count_occupied_neighbors(vertex, graph, min) do
    case :digraph.vertex(graph, vertex) do
      false ->
        raise "FUCK!"

      _ ->
        count =
          :digraph.in_neighbours(graph, vertex)
          |> Enum.uniq()
          |> Enum.reduce(0, fn cur, acc ->
            case occupied?(cur, graph) do
              true -> acc + 1
              false -> acc
            end
          end)

        cond do
          count == 0 -> :zero
          count >= min -> :four_or_more
          true -> nil
        end
    end
  end

  def occupied?(coords, graph) do
    {_, label} = :digraph.vertex(graph, coords)
    label[:occupied]
  end

  def count_occupied(graph) do
    :digraph.vertices(graph)
    |> Enum.reduce(0, fn cur, acc ->
      case occupied?(cur, graph) do
        true -> acc + 1
        false -> acc
      end
    end)
  end

  @doc """
  If a seat is empty (L) and there are no occupied seats adjacent to it, the seat becomes occupied.
  If a seat is occupied (#) and four or more seats adjacent to it are also occupied, the seat becomes empty.
  Otherwise, the seat's state does not change.
  """
  def change_for(coords, graph, min) do
    case {occupied?(coords, graph), count_occupied_neighbors(coords, graph, min)} do
      {false, :zero} ->
        {coords, true}

      {true, :four_or_more} ->
        {coords, false}

      {_, _} ->
        nil
    end
  end

  def get_changes(graph, min \\ 4) do
    :digraph.vertices(graph)
    |> Enum.map(&change_for(&1, graph, min))
    |> Enum.filter(& &1)
  end

  def cycle(graph, min \\ 4), do: cycle(graph, get_changes(graph, min), min)

  def cycle(graph, changes, min) when length(changes) > 0 do
    for {vertex, new_state} <- changes do
      :digraph.add_vertex(graph, vertex, occupied: new_state)
    end

    cycle(graph, get_changes(graph, min), min)
  end

  def cycle(graph, changes, _) when length(changes) == 0 do
    graph
  end

  @doc """
  ## Examples

      iex> part_1(testdata())
      37
  """
  def part_1(input) do
    graph = graphit(input)

    cycle(graph)

    count_occupied(graph)
  end

  @doc """
  ## Examples

      iex> part_2(testdata())
      26
  """
  def part_2(input) do
    graph = graphit_pt2(input)

    cycle(graph, 5)

    count_occupied(graph)
  end
end
