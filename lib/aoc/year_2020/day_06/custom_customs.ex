defmodule Aoc.Year2020.Day06.CustomCustoms do
  @moduledoc """
  ## --- Day 6: Custom Customs --- 

  As your flight approaches the regional airport where you'll switch to a much
  larger plane, customs declaration forms are distributed to the passengers. 

  The form asks a series of 26 yes-or-no questions marked `a` through `z`. All you
  need to do is identify the questions for which *anyone in your group* answers
  "yes". Since your group is just you, this doesn't take very long. 

  However, the person sitting next to you seems to be experiencing a language
  barrier and asks if you can help. For each of the people in their group, you
  write down the questions for which they answer "yes", one per line. For example: 

  `abcx
  abcy
  abcz
  `In this group, there are *`6`* questions to which anyone answered "yes": `a`,
  `b`, `c`, `x`, `y`, and `z`. (Duplicate answers to the same question don't count
  extra; each question counts at most once.) 

  Another group asks for your help, then another, and eventually you've collected
  answers from every group on the plane (your puzzle input). Each group's answers
  are separated by a blank line, and within each group, each person's answers are
  on a single line. For example: 

  `abc

  a
  b
  c

  ab
  ac

  a
  a
  a
  a

  b
  `This list represents answers from five groups: 

  - The first group contains one person who answered "yes" to *`3`* questions: `a`, `b`, and `c`. 
  - The second group contains three people; combined, they answered "yes" to *`3`* questions: `a`, `b`, and `c`. 
  - The third group contains two people; combined, they answered "yes" to *`3`* questions: `a`, `b`, and `c`. 
  - The fourth group contains four people; combined, they answered "yes" to only *`1`* question, `a`. 
  - The last group contains one person who answered "yes" to only *`1`* question, `b`. 
  In this example, the sum of these counts is `3 + 3 + 3 + 1 + 1` = *`11`*. 

  For each group, count the number of questions to which anyone answered "yes".
  *What is the sum of those counts?* 


  """

  def groups(input) do
    input
    |> String.split("\n\n")
    |> Enum.map(&String.graphemes/1)
  end

  @doc ~S"""
    iex> all_yes_in_group([["a", "b", "c"], ["a", "c"], ["a", "c"]])
    2
    iex> all_yes_in_group([["a", "b", "c"]])
    3
    iex> all_yes_in_group([["a", "b"], ["a", "c"], ["c"]])
    0
  """
  def all_yes_in_group(group) do
    total = length(group)

    all_answers =
      group
      |> List.flatten()
      |> Enum.uniq()

    all_yes =
      all_answers
      |> Enum.map(fn answer ->
        group
        |> Enum.reduce(0, fn cur, acc ->
          acc +
            Enum.find_value(
              cur,
              0,
              &if &1 == answer do
                1
              end
            )
        end)
      end)

    all_yes
    |> Enum.filter(&(&1 == total))
    |> length
  end

  def uniq_in_group(group) do
    group
    |> Enum.uniq()
    |> Enum.filter(&(&1 != "\n"))
    |> length
  end

  def sum_groups(group, total) do
    uniq_in_group(group) + total
  end

  def test_data(),
    do: """
    abc

    a
    b
    c

    ab
    ac

    a
    a
    a
    a

    b
    """

  @doc """
  Solves part 1

    iex> test_data() |> part_1()
    11

  """
  def part_1(input) do
    input
    |> groups
    |> Enum.reduce(0, &sum_groups/2)
  end

  @doc """

    iex> test_data() |> part_2()
    6

  """
  def part_2(input) do
    nice_groups =
      input
      |> String.split("\n\n")
      |> Enum.map(fn group ->
        group
        |> String.split("\n")
        |> Enum.map(fn answers ->
          answers
          |> String.graphemes()
          |> Enum.filter(&(&1 != "\n"))
        end)
        |> Enum.filter(&(length(&1) > 0))
      end)

    nice_groups
    |> Enum.reduce(0, fn cur, acc ->
      acc + all_yes_in_group(cur)
    end)
  end
end
