defmodule Aoc.Year2020.Day09.EncodingError do
  @moduledoc """
  ## --- Day 9: Encoding Error --- 

  With your neighbor happily enjoying their video game, you turn your attention to
  an open data port on the little screen in the seat in front of you. 

  Though the port is non-standard, you manage to connect it to your computer
  through the clever use of several paperclips. Upon connection, the port outputs
  a series of numbers (your puzzle input). 

  The data appears to be encrypted with the eXchange-Masking Addition System
  (XMAS) which, conveniently for you, is an old cypher with an important weakness. 

  XMAS starts by transmitting a *preamble* of 25 numbers. After that, each number
  you receive should be the sum of any two of the 25 immediately previous numbers.
  The two numbers will have different values, and there might be more than one
  such pair. 

  For example, suppose your preamble consists of the numbers `1` through `25` in a
  random order. To be valid, the next number must be the sum of two of those
  numbers: 

  - `26` would be a *valid* next number, as it could be `1` plus `25` (or many other pairs, like `2` and `24`). 
  - `49` would be a *valid* next number, as it is the sum of `24` and `25`. 
  - `100` would *not* be valid; no two of the previous 25 numbers sum to `100`. 
  - `50` would also *not* be valid; although `25` appears in the previous 25 numbers, the two numbers in the pair must be different. 
  Suppose the 26th number is `45`, and the first number (no longer an option, as
  it is more than 25 numbers ago) was `20`. Now, for the next number to be valid,
  there needs to be some pair of numbers among `1`-`19`, `21`-`25`, or `45` that
  add up to it: 

  - `26` would still be a *valid* next number, as `1` and `25` are still within the previous 25 numbers. 
  - `65` would *not* be valid, as no two of the available numbers sum to it. 
  - `64` and `66` would both be *valid*, as they are the result of `19+45` and `21+45` respectively. 
  Here is a larger example which only considers the previous *5* numbers (and has
  a preamble of length 5): 

  `35
  20
  15
  25
  47
  40
  62
  55
  65
  95
  102
  117
  150
  182
  127
  219
  299
  277
  309
  576
  `In this example, after the 5-number preamble, almost every number is the sum of
  two of the previous 5 numbers; the only number that does not follow this rule is
  *`127`*. 

  The first step of attacking the weakness in the XMAS data is to find the first
  number in the list (after the preamble) which is *not* the sum of two of the 25
  numbers before it. *What is the first number that does not have this property?* 


  """

  def combos(0, _), do: [[]]
  def combos(_, []), do: []

  def combos(n, [head | tail]) do
    for(l <- combos(n - 1, tail), do: [head | l]) ++ combos(n, tail)
  end

  @doc """
      iex> can_sum([[1,10], [1, 100], [10, 100]], 101)
      true
      iex> can_sum([[1,10], [1, 100], [10, 100]], 102)
      102
  """
  def can_sum(addends, sum)
  def can_sum([], sum), do: sum

  def can_sum([[a, b] | rest], sum) do
    case a + b == sum do
      true ->
        true

      false ->
        can_sum(rest, sum)
    end
  end

  def testdata(),
    do: """
    35
    20
    15
    25
    47
    40
    62
    55
    65
    95
    102
    117
    150
    182
    127
    219
    299
    277
    309
    576
    """

  def parse(data) do
    data
    |> String.split("\n")
    |> Enum.filter(&(&1 != ""))
    |> Enum.map(&String.to_integer/1)
  end

  @doc """

      iex> testdata() |> parse() |> xmas(5)
      127

  """
  def xmas(nums, preamble_length) do
    nums
    |> Enum.chunk_every(preamble_length + 1, 1, :discard)
    |> Enum.map(&can_sum(combos(2, &1), List.last(&1)))
    |> Enum.find(&(&1 != true))
  end

  def part_1(input) do
    input
    |> parse()
    |> xmas(25)
  end

  def sample_range(),
    do: """
    35
    20
    15
    25
    47
    40
    62
    55
    65
    95
    102
    117
    150
    182
    127
    219
    299
    277
    309
    576
    """

  def sum(range, l, r), do: Enum.sum(Enum.slice(range, l, r - l))

  @doc """
     iex> sample_range() |> parse() |> range_totals(127)
     62
  """
  def range_totals(range, target) do
    range_totals(0, 0, 1, range, sum(range, 0, 1), target)
  end

  def range_totals(ll, l, r, range, sum, target)
  def range_totals(_ll, l, r, range, sum, target) when sum == target, do: solution(range, l, r)

  def range_totals(ll, l, r, range, _sum, target) when r == length(range) do
    range_totals(ll, l + 1, l + 2, range, sum(range, l + 1, l + 2), target)
  end

  def range_totals(ll, l, r, range, _sum, target) do
    range_totals(ll, l, r + 1, range, sum(range, l, r + 1), target)
  end

  defp solution(range, l, r) do
    sub =
      Enum.slice(range, l, r - l)
      |> Enum.sort()

    List.first(sub) + List.last(sub)
  end

  @doc """

  """
  def part_2(input) do
    xmas = part_1(input)

    parsed = parse(input)
    range_totals(parsed, xmas)
  end
end
