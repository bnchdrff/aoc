defmodule Aoc.Year2020.Day12.RainRisk do
  @moduledoc """
  ## --- Day 12: Rain Risk --- 

  Your ferry made decent progress toward the island, but the storm came in faster
  than anyone expected. The ferry needs to take *evasive actions*! 

  Unfortunately, the ship's navigation computer seems to be malfunctioning; rather
  than giving a route directly to safety, it produced extremely circuitous
  instructions. When the captain uses the PA system to ask if anyone can help, you
  quickly volunteer. 

  The navigation instructions (your puzzle input) consists of a sequence of
  single-character *actions* paired with integer input *values*. After staring at
  them for a few minutes, you work out what they probably mean: 

  - Action *`N`* means to move *north* by the given value. 
  - Action *`S`* means to move *south* by the given value. 
  - Action *`E`* means to move *east* by the given value. 
  - Action *`W`* means to move *west* by the given value. 
  - Action *`L`* means to turn *left* the given number of degrees. 
  - Action *`R`* means to turn *right* the given number of degrees. 
  - Action *`F`* means to move *forward* by the given value in the direction the ship is currently facing. 
  The ship starts by facing *east*. Only the `L` and `R` actions change the
  direction the ship is facing. (That is, if the ship is facing east and the next
  instruction is `N10`, the ship would move north 10 units, but would still move
  east if the following action were `F`.) 

  For example: 

  `F10
  N3
  F7
  R90
  F11
  `These instructions would be handled as follows: 

  - `F10` would move the ship 10 units east (because the ship starts by facing east) to *east 10, north 0*. 
  - `N3` would move the ship 3 units north to *east 10, north 3*. 
  - `F7` would move the ship another 7 units east (because the ship is still facing east) to *east 17, north 3*. 
  - `R90` would cause the ship to turn right by 90 degrees and face *south*; it remains at *east 17, north 3*. 
  - `F11` would move the ship 11 units south to *east 17, south 8*. 
  At the end of these instructions, the ship's Manhattan distance (sum of the
  absolute values of its east/west position and its north/south position) from its
  starting position is `17 + 8` = *`25`*. 

  Figure out where the navigation instructions lead. *What is the Manhattan
  distance between that location and the ship's starting position?* 


  """

  def testdata(),
    do: """
    F10
    N3
    F7
    R90
    F11
    """

  defmodule Position do
    defstruct ew: nil, ns: nil, bearing: nil

    @type t(ew, ns, bearing) :: %Position{ew: ew, ns: ns, bearing: bearing}

    @type t :: %Position{ew: integer(), ns: integer(), bearing: integer()}
  end

  defmodule Instruction do
    defstruct op: nil, arg: nil

    @type t(op, arg) :: %Instruction{op: op, arg: arg}

    @type t :: %Instruction{op: String.t(), arg: integer()}
  end

  def parse(input) do
    {:ok, re} = Regex.compile("([[:alpha:]]*)([[:digit:]]*)")

    input
    |> String.split("\n")
    |> Enum.filter(&(&1 != ""))
    |> Enum.map(fn l ->
      [op, arg] = Regex.run(re, l, capture: :all_but_first)
      %Instruction{op: op, arg: String.to_integer(arg)}
    end)
  end

  def make_moves(input) do
    parsed = parse(input)

    make_moves(%Position{ew: 0, ns: 0, bearing: 90}, parsed)
  end

  def make_moves(position, moves)

  def make_moves(position, []) do
    position
  end

  def make_moves(%Position{} = position, [%Instruction{} = move | rest]) do
    next = next_position(position, move)
    make_moves(next, rest)
  end

  def next_position(%Position{ew: ew, ns: ns, bearing: bearing}, %Instruction{op: "N", arg: arg}) do
    %Position{ew: ew, ns: ns + arg, bearing: bearing}
  end

  def next_position(%Position{ew: ew, ns: ns, bearing: bearing}, %Instruction{op: "S", arg: arg}) do
    %Position{ew: ew, ns: ns - arg, bearing: bearing}
  end

  def next_position(%Position{ew: ew, ns: ns, bearing: bearing}, %Instruction{op: "E", arg: arg}) do
    %Position{ew: ew + arg, ns: ns, bearing: bearing}
  end

  def next_position(%Position{ew: ew, ns: ns, bearing: bearing}, %Instruction{op: "W", arg: arg}) do
    %Position{ew: ew - arg, ns: ns, bearing: bearing}
  end

  def next_position(%Position{ew: ew, ns: ns, bearing: bearing}, %Instruction{op: "F", arg: arg}) do
    case bearing do
      0 -> %Position{ew: ew, ns: ns + arg, bearing: bearing}
      90 -> %Position{ew: ew + arg, ns: ns, bearing: bearing}
      180 -> %Position{ew: ew, ns: ns - arg, bearing: bearing}
      270 -> %Position{ew: ew - arg, ns: ns, bearing: bearing}
    end
  end

  def next_position(%Position{ew: ew, ns: ns, bearing: bearing}, %Instruction{op: "L", arg: arg}) do
    %Position{ew: ew, ns: ns, bearing: rem(bearing + 360 - arg, 360)}
  end

  def next_position(%Position{ew: ew, ns: ns, bearing: bearing}, %Instruction{op: "R", arg: arg}) do
    %Position{ew: ew, ns: ns, bearing: rem(bearing + arg, 360)}
  end

  @doc """

      iex> part_1(testdata())
      25

  """
  def part_1(input) do
    %Position{ew: ew, ns: ns, bearing: _bearing} = make_moves(input)

    abs(ew) + abs(ns)
  end

  defmodule P2 do
    defstruct ew: nil, ns: nil

    @type t(ew, ns) :: %Position{ew: ew, ns: ns}

    @type t :: %Position{ew: integer(), ns: integer()}
  end

  def waypoint_style(input) do
    parsed = parse(input)

    waypoint_style(%P2{ew: 0, ns: 0}, %P2{ew: 10, ns: 1}, parsed)
  end

  def waypoint_style(position, waypoint, moves)

  def waypoint_style(position, _waypoint, []) do
    position
  end

  def waypoint_style(%P2{} = position, %P2{} = waypoint, [%Instruction{} = move | rest]) do
    {next_pos, next_way} = next_p2(position, waypoint, move)

    waypoint_style(next_pos, next_way, rest)
  end

  def next_p2(pos, way, %Instruction{
        op: "N",
        arg: arg
      }) do
    {pos, %P2{ew: way.ew, ns: way.ns + arg}}
  end

  def next_p2(pos, way, %Instruction{
        op: "S",
        arg: arg
      }) do
    {pos, %P2{ew: way.ew, ns: way.ns - arg}}
  end

  def next_p2(pos, way, %Instruction{
        op: "E",
        arg: arg
      }) do
    {pos, %P2{ew: way.ew + arg, ns: way.ns}}
  end

  def next_p2(pos, way, %Instruction{
        op: "W",
        arg: arg
      }) do
    {pos, %P2{ew: way.ew - arg, ns: way.ns}}
  end

  def next_p2(pos, way, %Instruction{
        op: "F",
        arg: arg
      }) do
    {%P2{ew: pos.ew + way.ew * arg, ns: pos.ns + way.ns * arg}, way}
  end

  def next_p2(pos, way, %Instruction{
        op: "R",
        arg: arg
      }) do
    case arg do
      90 -> {pos, %P2{ew: way.ns, ns: 0 - way.ew}}
      180 -> {pos, %P2{ew: way.ew * -1, ns: way.ns * -1}}
      270 -> {pos, %P2{ew: 0 - way.ns, ns: way.ew}}
    end
  end

  def next_p2(pos, way, %Instruction{
        op: "L",
        arg: arg
      }) do
    case arg do
      90 -> {pos, %P2{ew: 0 - way.ns, ns: way.ew}}
      180 -> {pos, %P2{ew: way.ew * -1, ns: way.ns * -1}}
      270 -> {pos, %P2{ew: way.ns, ns: 0 - way.ew}}
    end
  end

  @doc """

      iex> part_2(testdata())
      286

  """
  def part_2(input) do
    %P2{ew: ew, ns: ns} = waypoint_style(input)
    abs(ew) + abs(ns)
  end
end
