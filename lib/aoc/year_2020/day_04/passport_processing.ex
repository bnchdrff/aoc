defmodule Aoc.Year2020.Day04.PassportProcessing do
  @moduledoc """
  ## --- Day 4: Passport Processing --- 

  You arrive at the airport only to realize that you grabbed your North Pole
  Credentials instead of your passport. While these documents are extremely
  similar, North Pole Credentials aren't issued by a country and therefore aren't
  actually valid documentation for travel in most of the world. 

  It seems like you're not the only one having problems, though; a very long line
  has formed for the automatic passport scanners, and the delay could upset your
  travel itinerary. 

  Due to some questionable network security, you realize you might be able to
  solve both of these problems at the same time. 

  The automatic passport scanners are slow because they're having trouble
  *detecting which passports have all required fields*. The expected fields are as
  follows: 

  - `byr` (Birth Year) 
  - `iyr` (Issue Year) 
  - `eyr` (Expiration Year) 
  - `hgt` (Height) 
  - `hcl` (Hair Color) 
  - `ecl` (Eye Color) 
  - `pid` (Passport ID) 
  - `cid` (Country ID) 
  Passport data is validated in batch files (your puzzle input). Each passport is
  represented as a sequence of `key:value` pairs separated by spaces or newlines.
  Passports are separated by blank lines. 

  Here is an example batch file containing four passports: 

  `ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
  byr:1937 iyr:2017 cid:147 hgt:183cm

  iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
  hcl:#cfa07d byr:1929

  hcl:#ae17e1 iyr:2013
  eyr:2024
  ecl:brn pid:760753108 byr:1931
  hgt:179cm

  hcl:#cfa07d eyr:2025 pid:166559648
  iyr:2011 ecl:brn hgt:59in
  `The first passport is *valid* - all eight fields are present. The second
  passport is *invalid* - it is missing `hgt` (the Height field). 

  The third passport is interesting; the *only missing field* is `cid`, so it
  looks like data from North Pole Credentials, not a passport at all! Surely,
  nobody would mind if you made the system temporarily ignore missing `cid`
  fields. Treat this "passport" as *valid*. 

  The fourth passport is missing two fields, `cid` and `byr`. Missing `cid` is
  fine, but missing any other field is not, so this passport is *invalid*. 

  According to the above rules, your improved system would report `*2*` valid
  passports. 

  Count the number of *valid* passports - those that have all required fields.
  Treat `cid` as optional. *In your batch file, how many passports are valid?* 


  """

  def test_data(),
    do: """
    ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
    byr:1937 iyr:2017 cid:147 hgt:183cm

    iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
    hcl:#cfa07d byr:1929

    hcl:#ae17e1 iyr:2013
    eyr:2024
    ecl:brn pid:760753108 byr:1931
    hgt:179cm

    hcl:#cfa07d eyr:2025 pid:166559648
    iyr:2011 ecl:brn hgt:59in
    """

  def good_passports(),
    do: """
    pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
    hcl:#623a2f

    eyr:2029 ecl:blu cid:129 byr:1989
    iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

    hcl:#888785
    hgt:164cm byr:2001 iyr:2015 cid:88
    pid:545766238 ecl:hzl
    eyr:2022

    iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719
    """

  def bad_passports(),
    do: """
    eyr:1972 cid:100
    hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

    iyr:2019
    hcl:#602927 eyr:1967 hgt:170cm
    ecl:grn pid:012533040 byr:1946

    hcl:dab227 iyr:2012
    ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

    hgt:59cm ecl:zzz
    eyr:2038 hcl:74454a iyr:2023
    pid:3556412378 byr:2007
    """

  @doc ~S"""
  Separates a string of passport blobs into a list of maps

  ## Examples

      iex> test_data() |> parse_passports
      [
        %{"byr" => "1937", "cid" => "147", "ecl" => "gry", "eyr" => "2020", "hcl" => "#fffffd", "hgt" => "183cm", "iyr" => "2017", "pid" => "860033327"},
        %{"byr" => "1929", "cid" => "350", "ecl" => "amb", "eyr" => "2023", "hcl" => "#cfa07d", "iyr" => "2013", "pid" => "028048884"},
        %{"byr" => "1931", "ecl" => "brn", "eyr" => "2024", "hcl" => "#ae17e1", "hgt" => "179cm", "iyr" => "2013", "pid" => "760753108"},
        %{"ecl" => "brn", "eyr" => "2025", "hcl" => "#cfa07d", "hgt" => "59in", "iyr" => "2011", "pid" => "166559648"}
      ]
      
  """
  def parse_passports(blob) do
    blob
    |> String.split("\n\n")
    |> Enum.map(&__MODULE__.parse_passport/1)
  end

  @doc ~S"""
  Turns a single passport multiline string into a map
  """
  def parse_passport(blob) do
    blob
    |> String.split()
    |> Enum.map(&(String.split(&1, ":") |> List.to_tuple()))
    |> Map.new()
  end

  @doc ~S"""
  Validates a passport

  ## Examples

      iex> test_data() |> parse_passports |> Enum.map(&valid?(&1))
      [true, false, true, false]

  """
  def valid?(passport) do
    ["hcl", "iyr", "eyr", "ecl", "pid", "byr", "hgt"]
    |> Enum.all?(&Map.has_key?(passport, &1))
  end

  @doc ~S"""
  Validates a passport for part 2

  ## Examples

      iex> good_passports() |> parse_passports |> Enum.map(&really_valid?(&1))
      [true, true, true, true]
      iex> bad_passports() |> parse_passports |> Enum.map(&really_valid?(&1))
      [false, false, false, false]

  """
  def really_valid?(passport) do
    byr =
      Map.get(passport, "byr", "-1")
      |> num_range(1920, 2002)

    iyr =
      Map.get(passport, "iyr", "-1")
      |> num_range(2010, 2020)

    eyr =
      Map.get(passport, "eyr", "-1")
      |> num_range(2020, 2030)

    hgt =
      Map.get(passport, "hgt", "0")
      |> valid_hgt

    ecl =
      Map.get(passport, "ecl", "xxx")
      |> valid_ecl

    hcl =
      Map.get(passport, "hcl", "xxx")
      |> String.match?(~r/^#[0-9a-f]{6}$/)

    pid =
      Map.get(passport, "pid", "xxx")
      |> String.match?(~r/^[0-9]{9}$/)

    Enum.all?([hcl, iyr, eyr, ecl, pid, byr, hgt])
  end

  defp num_range(n, min, max) do
    {i, _} = Integer.parse(n)
    i >= min and i <= max
  end

  defp valid_ecl(s) do
    Enum.any?(["amb", "blu", "brn", "gry", "grn", "hzl", "oth"], &(&1 == s))
  end

  defp valid_hgt(s) do
    cond do
      String.match?(s, ~r/cm$/) -> valid_cm(s)
      String.match?(s, ~r/in$/) -> valid_in(s)
      true -> false
    end
  end

  # If cm, the number must be at least 150 and at most 193.
  defp valid_cm(s) do
    {height, _} = Regex.run(~r/[0-9]*/, s) |> List.first() |> Integer.parse()

    height >= 150 and height <= 193
  end

  # If in, the number must be at least 59 and at most 76.
  defp valid_in(s) do
    {height, _} = Regex.run(~r/[0-9]*/, s) |> List.first() |> Integer.parse()

    height >= 59 and height <= 76
  end

  @doc """


  """
  def part_1(input) do
    input
    |> parse_passports
    |> Enum.map(&valid?(&1))
    |> Enum.filter(& &1)
    |> length
  end

  @doc """

  """
  def part_2(input) do
    input
    |> parse_passports
    |> Enum.map(&really_valid?(&1))
    |> Enum.filter(& &1)
    |> length
  end
end
