defmodule Aoc.Year2020.Day07.HandyHaversacks do
  @moduledoc """
  ## --- Day 7: Handy Haversacks --- 

  You land at the regional airport in time for your next flight. In fact, it looks
  like you'll even have time to grab some food: all flights are currently delayed
  due to *issues in luggage processing*. 

  Due to recent aviation regulations, many rules (your puzzle input) are being
  enforced about bags and their contents; bags must be color-coded and must
  contain specific quantities of other color-coded bags. Apparently, nobody
  responsible for these regulations considered how long they would take to
  enforce! 

  For example, consider the following rules: 

  `light red bags contain 1 bright white bag, 2 muted yellow bags.
  dark orange bags contain 3 bright white bags, 4 muted yellow bags.
  bright white bags contain 1 shiny gold bag.
  muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
  shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
  dark olive bags contain 3 faded blue bags, 4 dotted black bags.
  vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
  faded blue bags contain no other bags.
  dotted black bags contain no other bags.
  `These rules specify the required contents for 9 bag types. In this example,
  every `faded blue` bag is empty, every `vibrant plum` bag contains 11 bags (5
  `faded blue` and 6 `dotted black`), and so on. 

  You have a `*shiny gold*` bag. If you wanted to carry it in at least one other
  bag, how many different bag colors would be valid for the outermost bag? (In
  other words: how many colors can, eventually, contain at least one `shiny gold`
  bag?) 

  In the above rules, the following options would be available to you: 

  - A `bright white` bag, which can hold your `shiny gold` bag directly. 
  - A `muted yellow` bag, which can hold your `shiny gold` bag directly, plus some other bags. 
  - A `dark orange` bag, which can hold `bright white` and `muted yellow` bags, either of which could then hold your `shiny gold` bag. 
  - A `light red` bag, which can hold `bright white` and `muted yellow` bags, either of which could then hold your `shiny gold` bag. 
  So, in this example, the number of bag colors that can eventually contain at
  least one `shiny gold` bag is `*4*`. 

  *How many bag colors can eventually contain at least one `shiny gold` bag?* (The
  list of rules is quite long; make sure you get all of it.) 

  ## --- Part Two --- 

  It's getting pretty expensive to fly these days - not because of ticket prices,
  but because of the ridiculous number of bags you need to buy! 

  Consider again your `shiny gold` bag and the rules from the above example: 

  - `faded blue` bags contain `0` other bags. 
  - `dotted black` bags contain `0` other bags. 
  - `vibrant plum` bags contain `11` other bags: 5 `faded blue` bags and 6 `dotted black` bags. 
  - `dark olive` bags contain `7` other bags: 3 `faded blue` bags and 4 `dotted black` bags. 
  So, a single `shiny gold` bag must contain 1 `dark olive` bag (and the 7 bags
  within it) plus 2 `vibrant plum` bags (and the 11 bags within *each* of those):
  `1 + 1*7 + 2 + 2*11` = `*32*` bags! 

  Of course, the actual rules have a small chance of going several levels deeper
  than this example; be sure to count all of the bags, even if the nesting becomes
  topologically impractical! 

  Here's another example: 

  `shiny gold bags contain 2 dark red bags.
  dark red bags contain 2 dark orange bags.
  dark orange bags contain 2 dark yellow bags.
  dark yellow bags contain 2 dark green bags.
  dark green bags contain 2 dark blue bags.
  dark blue bags contain 2 dark violet bags.
  dark violet bags contain no other bags.
  `In this example, a single `shiny gold` bag must contain `*126*` other bags. 

  *How many individual bags are required inside your single `shiny gold` bag?* 


  """

  def testdata(),
    do: """
    light red bags contain 1 bright white bag, 2 muted yellow bags.
    dark orange bags contain 3 bright white bags, 4 muted yellow bags.
    bright white bags contain 1 shiny gold bag.
    muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
    shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
    dark olive bags contain 3 faded blue bags, 4 dotted black bags.
    vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
    faded blue bags contain no other bags.
    dotted black bags contain no other bags.
    """

  def testdata_2(),
    do: """
    shiny gold bags contain 2 dark red bags.
    dark red bags contain 2 dark orange bags.
    dark orange bags contain 2 dark yellow bags.
    dark yellow bags contain 2 dark green bags.
    dark green bags contain 2 dark blue bags.
    dark blue bags contain 2 dark violet bags.
    dark violet bags contain no other bags.
    """

  @doc """
  Produce a graphable map based on the input string

  ## Examples

      iex> parse("light red bags contain 1 bright white bag, 2 muted yellow bags.")
      {
        :"light red",
        [
          {1, :"bright white"},
          {2, :"muted yellow"},
        ]
      }
  """
  def parse(line) do
    [color, rest] = Regex.run(~r/^(.*) bags contain (.*)./, line, capture: :all_but_first)

    rest =
      String.split(rest, ",")
      |> Enum.filter(&(!String.contains?(&1, "no ")))
      |> Enum.map(&extract_children(&1))

    {String.to_atom(color), rest}
  end

  def extract_children(str) do
    [count, color] =
      Regex.run(~r/^([[:digit:]*]) (.*) bag*/, String.trim(str), capture: :all_but_first)

    {int_count, _} = Integer.parse(count)

    {int_count, String.to_atom(color)}
  end

  @doc """
  Add a given color to a graph
  """
  def add_to_graph({from, rest}, graph) do
    for {count, to} <- rest, do: :digraph.add_edge(graph, from, to, count)
  end

  @doc """
  Create a graph with color nodes based on the input data
  """
  def create_graph(input) do
    parsed =
      input
      |> String.split("\n")
      |> Enum.filter(&("" != &1))
      |> Enum.map(&parse/1)

    g = :digraph.new()

    # Add all colors as vertices
    for color <- Keyword.keys(parsed), do: :digraph.add_vertex(g, color)

    # Add verticies
    for spec <- parsed, do: add_to_graph(spec, g)

    g
  end

  # foo
  def dfs(graph, vertex, acc \\ []) do
    case vertex in acc do
      false ->
        acc = [vertex | acc]

        Enum.reduce(:digraph.out_neighbours(graph, vertex), acc, fn elem, acc ->
          dfs(graph, elem, acc)
        end)

      true ->
        acc
    end
  end

  def count_bags(g, parent) do
    edges = :digraph.out_edges(g, parent)

    case edges do
      [] ->
        0

      _ ->
        counts =
          for e <- edges, into: [] do
            {_edge, _parent, child, count} = :digraph.edge(g, e)
            count + count * count_bags(g, child)
          end

        Enum.sum(counts)
    end
  end

  @doc """
  Solves part 1

  ## Examples

      iex> testdata() |> part_1()
      4

  """
  def part_1(input) do
    g = create_graph(input)

    :digraph_utils.reaching([:"shiny gold"], g)
    |> Enum.filter(&(&1 != :"shiny gold"))
    |> length
  end

  @doc """

  ## Examples

      iex> testdata() |> part_2()
      32
      iex> testdata_2() |> part_2()
      126

  """
  def part_2(input) do
    create_graph(input) |> count_bags(:"shiny gold")
  end
end
