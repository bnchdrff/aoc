defmodule Aoc.Year2020.Day03.TobogganTrajectory do
  @moduledoc """
  ## --- Day 3: Toboggan Trajectory --- 

  With the toboggan login problems resolved, you set off toward the airport. While
  travel by toboggan might be easy, it's certainly not safe: there's very minimal
  steering and the area is covered in trees. You'll need to see which angles will
  take you near the fewest trees. 

  Due to the local geology, trees in this area only grow on exact integer
  coordinates in a grid. You make a map (your puzzle input) of the open squares
  (`.`) and trees (`#`) you can see. For example: 

  `..##.......
  #...#...#..
  .#....#..#.
  ..#.#...#.#
  .#...##..#.
  ..#.##.....
  .#.#.#....#
  .#........#
  #.##...#...
  #...##....#
  .#..#...#.#
  `These aren't the only trees, though; due to something you read about once
  involving arboreal genetics and biome stability, the same pattern repeats to the
  right many times: 

  `*..##.......*..##.........##.........##.........##.........##.......  --->
  *#...#...#..*#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..
  *.#....#..#.*.#....#..#..#....#..#..#....#..#..#....#..#..#....#..#.
  *..#.#...#.#*..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#
  *.#...##..#.*.#...##..#..#...##..#..#...##..#..#...##..#..#...##..#.
  *..#.##.....*..#.##.......#.##.......#.##.......#.##.......#.##.....  --->
  *.#.#.#....#*.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#
  *.#........#*.#........#.#........#.#........#.#........#.#........#
  *#.##...#...*#.##...#...#.##...#...#.##...#...#.##...#...#.##...#...
  *#...##....#*#...##....##...##....##...##....##...##....##...##....#
  *.#..#...#.#*.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#  --->
  `You start on the open square (`.`) in the top-left corner and need to reach the
  bottom (below the bottom-most row on your map). 

  The toboggan can only follow a few specific slopes (you opted for a cheaper
  model that prefers rational numbers); start by *counting all the trees* you
  would encounter for the slope *right 3, down 1*: 

  From your starting position at the top-left, check the position that is right 3
  and down 1. Then, check the position that is right 3 and down 1 from there, and
  so on until you go past the bottom of the map. 

  The locations you'd check in the above example are marked here with `*O*` where
  there was an open square and `*X*` where there was a tree: 

  `..##.........##.........##.........##.........##.........##.......  --->
  #..*O*#...#..#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..
  .#....*X*..#..#....#..#..#....#..#..#....#..#..#....#..#..#....#..#.
  ..#.#...#*O*#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#
  .#...##..#..*X*...##..#..#...##..#..#...##..#..#...##..#..#...##..#.
  ..#.##.......#.*X*#.......#.##.......#.##.......#.##.......#.##.....  --->
  .#.#.#....#.#.#.#.*O*..#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#
  .#........#.#........*X*.#........#.#........#.#........#.#........#
  #.##...#...#.##...#...#.*X*#...#...#.##...#...#.##...#...#.##...#...
  #...##....##...##....##...#*X*....##...##....##...##....##...##....#
  .#..#...#.#.#..#...#.#.#..#...*X*.#.#..#...#.#.#..#...#.#.#..#...#.#  --->
  `In this example, traversing the map using this slope would cause you to
  encounter `*7*` trees. 

  Starting at the top-left corner of your map and following a slope of right 3 and
  down 1, *how many trees would you encounter?* 


  """

  def collision(template, move_number, mod, steps) do
    row =
      String.split(template)
      |> Enum.at(move_number * steps)

    char_pos = Integer.mod(move_number * mod, String.length(row))

    bool_to_int(String.at(row, char_pos) == "#")
  end

  def bool_to_int(false), do: 0
  def bool_to_int(true), do: 1

  defp max_moves(template, steps) do
    rows =
      template
      |> String.split()
      |> length

    Integer.floor_div(rows, steps)
  end

  def collision_counter(_, move_number, max, _, _, collisions) when move_number >= max do
    collisions
  end

  def collision_counter(template, move_number, max, mod, steps, collisions) do
    collisions = collisions + collision(template, move_number, mod, steps)
    collision_counter(template, move_number + 1, max, mod, steps, collisions)
  end

  def part_1(input) do
    mod = 3
    steps = 1
    collision_counter(input, 1, max_moves(input, steps), mod, steps, 0)
  end

  @doc """
    Right 1, down 1.
    Right 3, down 1. (This is the slope you already checked.)
    Right 5, down 1.
    Right 7, down 1.
    Right 1, down 2.
  """
  def part_2(input) do
    one = collision_counter(input, 1, max_moves(input, 1), 1, 1, 0)
    two = collision_counter(input, 1, max_moves(input, 1), 3, 1, 0)
    three = collision_counter(input, 1, max_moves(input, 1), 5, 1, 0)
    four = collision_counter(input, 1, max_moves(input, 1), 7, 1, 0)
    five = collision_counter(input, 1, max_moves(input, 2), 1, 2, 0)

    one * two * three * four * five
  end
end
